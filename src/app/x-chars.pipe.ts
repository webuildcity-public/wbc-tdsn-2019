import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'xChars'
})
export class XCharsPipe implements PipeTransform {

  transform(value: string, args?: any): any {
    args = args || 1;
    return value.slice(0, args);
  }

}
