import { BrowserModule } from '@angular/platform-browser';
import { RouterModule, Routes } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { NgxMapboxGLModule } from 'ngx-mapbox-gl';

import { AppComponent } from './app.component';
import { NgForObjPipe } from './ng-for-obj.pipe';
import { XCharsPipe } from './x-chars.pipe';

const appRoutes: Routes = [
  { path: '', component: AppComponent }
];

const routing = RouterModule.forRoot(appRoutes);

@NgModule({
  declarations: [
    AppComponent,
    NgForObjPipe,
    XCharsPipe
  ],
  imports: [
    BrowserModule,
    RouterModule, routing,
    HttpClientModule,
    FormsModule,
    NgxMapboxGLModule.forRoot({
      accessToken: 'pk.eyJ1IjoidGFnZGVyc3RhZHRuYXR1ciIsImEiOiJjamZsN2JjMzcwaGJmMnFteHd1ZzloM3VjIn0.yFm68I8jcuSYhYoUodWuTA' // Can also be set per map (accessToken input of mgl-map)
    })
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
