import { Component, OnInit } from '@angular/core';
// import { Data } from "./../assets/LTdSN-Koordinaten-2018-final_2TM-tok";
import  { Data } from "./../data";
// let Data = require('./../data');
import { ReplaySubject } from 'rxjs/ReplaySubject';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { HttpClient } from '@angular/common/http';
import { ActivatedRoute, Router } from '@angular/router';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/do';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})

// BUG on clear search  
  
export class AppComponent implements OnInit {

  data;
  dataSource: BehaviorSubject<any> = new BehaviorSubject(null);
  // data$ = this.dataSource.asObservable();

  cursorStyle;
  mapInstance;
  mapCenter = [9.978566078558401, 53.55312496529285];
  mapZoom = [8];

  favExplain = true;

  categories = [
    { value: "Wanderung", selected: false, name: "Kategorie" },
    { value: "Sonstiges", selected: false, name: "Kategorie" },
    { value: "Geo Tag der Natur", selected: false, name: "Kategorie" },
    { value: "Schiffs-/Kanutour", selected: false, name: "Kategorie" },
    { value: "Kinderprogramm", selected: false, name: "Kategorie" },
    { value: "Fahrradtour", selected: false, name: "Kategorie" }
  ];

  icons = {
    "Wanderung": "blop_wanderung.png",
    "Sonstiges": "blop_sonstiges.png",
    "Geo Tag der Natur": "blop_geotag.png",
    "Schiffs-/Kanutour": "blop_boot.png",
    "Kinderprogramm": "blop_kind.png",
    "Fahrradtour": "blop_rad.png"
  };

  criteria = [
    { value: "Ja", selected: false, name: "Rollstuhl", displayName: "Barrierefrei" },
    { value: "Ja", selected: false, name: "WC", displayName: "WC vorhanden" },
    { value: "Ja", selected: false, name: "Hoergeschaedigt", displayName: "für Hörgeschädigte" },
    { value: "Ja", selected: false, name: "Hunde", displayName: "Hunde erlaubt" },
    { value: "Ja", selected: false, name: "Sehbehinderte-Sonderfuehrungen", displayName: "Sehbehinderte-Sonderführung" },
    { value: "Ja", selected: false, name: "Essen", displayName: "Angebot an Essen und Trinken" },
    { value: "Ja", selected: false, name: "Sehbehinderte", displayName: "für Sehbehinderte" }
  ];

  days = [
    { value: "Freitag", selected: false, name: "Tag", longName: "Freitag, 15. Juni" },
    { value: "Samstag", selected: false, name: "Tag", longName: "Samstag, 16. Juni" },
    { value: "Sonntag", selected: false, name: "Tag", longName: "Sonntag, 17. Juni" }
  ];

  times = [
    { value: "Vormittags", selected: false, name: "Anfangszeit", min: "-1", max: "12" },
    { value: "Nachmittags", selected: false, name: "Anfangszeit", min: "12", max: "18" },
    { value: "Abends", selected: false, name: "Anfangszeit", min: "18", max: "99" },
  ];

  // for disselectAll()
  filters = {
    "categories": this.categories,
    "criteria": this.criteria,
    "days": this.days,
    "times": this.times
  };
  
  textFilter = "";          // textsearch

  selectedPoints = null;    // selected via click marker or search
  openedPoints = [];        // after click on "alle details" in marker-popup and favedPoints
  activeTabIndex = 0;       // currently displayed point of openedPoints, for display in tabContent
  favedPoints = [];         // dublicate of openedPoints but faved only, for local storage
  openedPointOnLoad = null; // load via url query parameter ?id=abc

  popupActiveIndex = 0;     // if multiple points in popup, which was clicked
  iconsText = false;

  // mobile
  sideP_onSide = false;
  bottomP_onBottom = false;
  
  // mgl-image onLoad() when all => map.resize() to fix bug of not appearing / not clickable icons
  blops_loaded = {
    blop1_loaded: false,
    blop2_loaded: false,
    blop3_loaded: false,
    blop4_loaded: false,
    blop5_loaded: false,
    blop6_loaded: false,
    blop7_loaded: false
  }
  
  trackByIndex(index: number, obj: any): any {
    return index;
  }

  constructor(private http: HttpClient, private route: ActivatedRoute, private router: Router) {
    this.http.get("/data.json").subscribe((srcData: any[]) => {
      console.log(srcData);
      
      const data = srcData.filter(evt => evt.Status === "Fertig - Freigegeben");
      this.data = FeatureCollectionMaker.create(data);


      if (this.storageAvailable('localStorage')) {
        const stored = JSON.parse(localStorage.getItem("favedPoints"));
        
        if (stored !== null) {
          const restoredPoints = [];
          stored.forEach(favId => {
            const p = this.data.features.find(point => {
              return point.properties.Veranstaltungscode == favId;
            })
            p['faved'] = true;
            
            this.getRemainingTicketsQuantity(p.properties.Veranstaltungscode).then(res => {
              p["ticketStatus"] = res;
            })
            .catch(error => {
              // p["ticketStatus"] = "error";
              // console.log("error fetching ticket status");
            });
            restoredPoints.push(p);
          });
          
          this.favedPoints = stored;
          this.openedPoints = restoredPoints;
        }
      }
  
      this.route
        .queryParams
        .subscribe(params => {
          if (params.hasOwnProperty('id')) {
            const p = this.data.features.find(point => {
              return point.properties.Veranstaltungscode == params.id;
            })
            if (typeof p !== 'undefined') {
              this.openedPointOnLoad = p;
            }
          }
        });
    });


  }
  
  ngOnInit () {

  }

  onMapLoaded(mapInstance) {
    console.log("map loaded");
    
    this.mapInstance = mapInstance;
    if (this.openedPointOnLoad !== null) {
      this.reopenPopup(this.openedPointOnLoad);
      this.setActiveTab(0);
    }
  }

  blopsLoaded(blop) {
    this.blops_loaded[blop] = true;
    const blobs = Object.keys(this.blops_loaded);
    if (blobs.every(blopp => this.blops_loaded[blopp] === true)) {
      setTimeout(() => {
        this.dataSource.next(this.data);
        this.mapInstance.resize();
      }, 300);
    };
  }

  onClick(evt) {
    this.popupActiveIndex = 0;
    const coords = evt.hasOwnProperty('lngLat') ? evt.lngLat : { lng: evt.features[0].geometry.coordinates[0], lat: evt.features[0].geometry.coordinates[1] };
    
    setTimeout(() => {    // force popup remove (see https://github.com/Wykks/ngx-mapbox-gl/issues/8)
      this.selectedPoints = null;      
    }, 0);
    
    if (evt.features[0].properties.hasOwnProperty('cluster')) {
      this.mapFlyTo(coords, this.mapInstance.transform._zoom + 2);
    } else {
      this.mapFlyTo(coords);

      setTimeout(() => {
        this.selectedPoints = (<any>evt).features;
      }, 0);
      
      this.appendQueryParams({'id': (<any>evt).features[this.popupActiveIndex].properties.Veranstaltungscode});
    }
  }

  onPopupClose() {
    this.popupActiveIndex = 0;
    this.appendQueryParams(null);
  }

  onPopupCardClick(activeCardIndex) {
    this.popupActiveIndex = activeCardIndex;
    this.appendQueryParams({'id': this.selectedPoints[activeCardIndex].properties.Veranstaltungscode});
  }

  reopenPopup(reopenedPoint) {
    setTimeout(() => {    // force popup remove (see https://github.com/Wykks/ngx-mapbox-gl/issues/8)
      this.selectedPoints = null;
    }, 0);
    setTimeout(() => {
      this.selectedPoints = [reopenedPoint];
    }, 0);

    const coords = reopenedPoint.geometry.coordinates;
    this.mapFlyTo({ lng: coords[0], lat: coords[1] });
    this.appendQueryParams({'id': reopenedPoint.properties.Veranstaltungscode});
  }

  openPoint(openedPoint) {
    this.getRemainingTicketsQuantity(openedPoint.properties.Veranstaltungscode).then(res => {
      openedPoint["ticketStatus"] = res;
    })
    .catch(error => {
      // openedPoint["ticketStatus"] = "error";
      // console.log("error fetching ticket status");
    });
    
    const i = this.openedPoints.findIndex(p => p.properties.Veranstaltungscode === openedPoint.properties.Veranstaltungscode);
    if ( i == -1) {
      if (this.openedPoints[0] && this.openedPoints[0]['faved']) {
        this.openedPoints.splice(0, 0, openedPoint);
      } else {
        this.openedPoints[0] = openedPoint;
      }
      this.setActiveTab(0);
    } else {
      this.setActiveTab(i);
    }
    this.bottomP_onBottom = false;
  }

  appendQueryParams(queryParams) {
    this.router.navigate([''], {
      queryParams: queryParams,
      replaceUrl: true
    });
  }

  saveFav(point) {
    const i = this.openedPoints.findIndex(p => p.properties.Veranstaltungscode === point.properties.Veranstaltungscode);
    this.openedPoints[i]['faved'] = true;
    this.favedPoints.push(point.properties.Veranstaltungscode);
    this.saveToLocalStorage();
  }

  removeFav(point) {
    const i = this.openedPoints.findIndex(p => p.properties.Veranstaltungscode === point.properties.Veranstaltungscode);
    const iFav = this.favedPoints.findIndex(p => p === point.properties.Veranstaltungscode);
    delete this.openedPoints[i]['faved'];
    this.openedPoints.splice(i, 1);
    this.favedPoints.splice(iFav, 1);
    this.saveToLocalStorage();
    this.setActiveTab(this.openedPoints.length - 1);
  }

  saveToLocalStorage() {
    if (this.storageAvailable('localStorage')) {
      if (this.favedPoints.length > 0) {
        localStorage.setItem("favedPoints", JSON.stringify(this.favedPoints));
      } else {
        localStorage.removeItem("favedPoints");
      }
    }
  }

  tabClose(closedTabIndex) {
    let newActiveTabIndex = this.activeTabIndex - 1;
    newActiveTabIndex = newActiveTabIndex > 0 ? newActiveTabIndex : 0;
    this.setActiveTab(newActiveTabIndex);

    const newOpenedPoints = this.openedPoints.slice(0);
    newOpenedPoints.splice(closedTabIndex, 1);
    this.openedPoints = newOpenedPoints;
  }

  setActiveTab(newI) {
    this.activeTabIndex = newI;
  }

  mapFlyTo(coords: {lng: number, lat: number}, zoom?: number) {
    zoom = zoom || this.mapInstance.transform._zoom;
    this.mapInstance.flyTo({ center: coords, zoom: zoom })
  }
  
  zommHH() {
    this.mapInstance.flyTo({ center: { lng: this.mapCenter[0], lat: this.mapCenter[1] }, zoom: 8, bearing: 0, pitch: 0 })
  }

  getSelectedCategories() {
    return this.categories
      .filter(opt => opt.selected)
  }

  getSelectedCriteria() {
    return this.criteria
      .filter(opt => opt.selected);
  }

  getSelectedDays() {
    return this.days
      .filter(opt => opt.selected);
  }

  getSelectedTimes() {
    return this.times
      .filter(opt => opt.selected);
  }

  disselectAll(trueFalse) {
    trueFalse = trueFalse || false;
    Object.keys(this.filters).forEach(filterKey => {
      this.filters[filterKey].forEach(filter => {
        filter.selected = trueFalse;
      });
    })
  }

  filterByFlags() {
    const selectedCategories = this.getSelectedCategories();
    const selectedCriteria = this.getSelectedCriteria();
    const selectedDays = this.getSelectedDays();
    const selectedTimes = this.getSelectedTimes();

    const clone = JSON.parse(JSON.stringify(this.data));
    
    if (selectedCategories.length > 0) {
      clone.features = clone.features.filter(feature => {
        return selectedCategories.some(catKey => feature.properties[catKey.name] == catKey.value);
      });
    }

    if (selectedCriteria.length > 0) {
      clone.features = clone.features.filter(feature => {
        return selectedCriteria.every(critKey => feature.properties[critKey.name] == critKey.value)
      });
    }

    if (selectedDays.length > 0) {
      clone.features = clone.features.filter(feature => {
        return selectedDays.some(critKey => feature.properties[critKey.name] == critKey.value)
      });
    }

    if (selectedTimes.length > 0) {
      clone.features = clone.features.filter(feature => {
        return selectedTimes.some(critKey => {
          const startTime = feature.properties[critKey.name].split(":")[0];
          return startTime > critKey.min && startTime < critKey.max
        })
      });
    }

    this.dataSource.next(clone);
  }

  filterByText() {
    this.disselectAll(false);
    const clone = JSON.parse(JSON.stringify(this.data));
    
    const data = clone.features.filter(feature => {
      if (feature.properties["Veranstaltungscode"].toLowerCase().includes(this.textFilter.toLowerCase()) ||
        feature.properties["Titel"].toLowerCase().includes(this.textFilter.toLowerCase()) ||
        feature.properties["Beschreibungstext"].toLowerCase().includes(this.textFilter.toLowerCase()) ||
        feature.properties["Besonderheiten_Anmeldung"].toLowerCase().includes(this.textFilter.toLowerCase()) ||
        feature.properties["Treffpunkt"].toLowerCase().includes(this.textFilter.toLowerCase()) ||
        feature.properties["Anfahrt"].toLowerCase().includes(this.textFilter.toLowerCase()) ||
        feature.properties["Veranstalter"].toLowerCase().includes(this.textFilter.toLowerCase()) ||
        feature.properties["Leitung"].toLowerCase().includes(this.textFilter.toLowerCase()) ||
        feature.properties["Ausruestung"].toLowerCase().includes(this.textFilter.toLowerCase())
      ) {
        return true
      } else {
        return false
      }
    });
    clone.features = data;
    this.dataSource.next(clone);
  }

  storageAvailable(type) {
    try {
      var storage = window[type],
        x = '__storage_test__';
      storage.setItem(x, x);
      storage.removeItem(x);
      return true;
    }
    catch (e) {
      return e instanceof DOMException && (
        // everything except Firefox
        e.code === 22 ||
        // Firefox
        e.code === 1014 ||
        // test name field too, because code might not be present
        // everything except Firefox
        e.name === 'QuotaExceededError' ||
        // Firefox
        e.name === 'NS_ERROR_DOM_QUOTA_REACHED') &&
        // acknowledge QuotaExceededError only if there's something already stored
        storage.length !== 0;
    }
  }

  getRemainingTicketsQuantity(eventId) {
    return new Promise((resolve, reject) => {
      // const url = "https://tagderstadtnaturhamburg.de/kartenstatus&nummer=AM101";
      const url = "https://tagderstadtnaturhamburg.de/kartenstatus&nummer=" + eventId;
      this.http.get(url).subscribe(res => {
        let status;
        res = Number(res);
        
        if (res === 0) {
          status = "Noch Karten vorhanden";
        } else if (res === 1) {
          status = "Nur noch wenige Karten vorhanden";
        } else if (res === 2) {
          status = "Leider ausgebucht";
        } else if (res === 3) {
          status = "Storniert";
        }
        resolve(status);
      }, error => {
        reject(error);
      })
    })
  }
}

class FeatureCollectionMaker {
  static create(features: any[]) {
    const collection: GeoJSON.FeatureCollection<GeoJSON.Point> = {
      "type": "FeatureCollection",
      "features": []
    };

    features.forEach(feature => {
      collection.features.push({
        "type": "Feature",
        "geometry": {
          "type": "Point",
          "coordinates": feature["WGS-Koordinaten"].split(/\s*,\s*/).reverse()
        },
        "properties": feature
      })
    })
    return collection;
  }
}